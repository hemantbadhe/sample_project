from django.shortcuts import render


def index(request):
    return render(request, 'index.html')


def error_404_view(request, exception):
    data = {"name": "ThePythonDjango.com"}
    return render(request, 'error_404.html', data)
