from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('', include('test_app.urls')),
    path('second_app/', include('second_app.urls')),
    path('admin/', admin.site.urls),
]
handler404 = 'test_app.views.error_404_view'
